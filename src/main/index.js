let fs = require('fs'),
    path = require('path'),
    mime = require('mime');

let FileServer = require('./http_server/file_server').FileServer;

const PORT = 7070;
const ROOT_PATH = '../resource';

let fileServer = new FileServer().listen(PORT);

fileServer.on('processRequest', function (req, res) {
    console.log(' process request ' );
    if (req.method !== 'GET') {
        console.log(' process unsupported method ' );
        res.statusCode = 405;
        res.end();
    } else {
        console.log(' process reading file ' );
        readFile(req, res);
    }
});

function readFile(req, res) {
    let pathname = path.normalize(ROOT_PATH + req.uri);
    fs.stat(pathname, function (err, stats) {
        if (err) {
            console.log(' error during open file ', err, pathname);
            res.statusCode = 404;
            res.end();
        } else if (stats.isFile()) {
            let type = mime.getType(pathname);
            let file = fs.createReadStream(pathname, {
                highWaterMark: 512
            });

            file.on('open', function () {
                res.statusCode = 200;
                res.writeHead('Content-Type', type);
                res.writeHead('Connection', 'close');
                res.writeFile(file);
                res.end();
            });

            file.on('close', function () {
                console.log(' closed file ', pathname );
            });

            file.on('error', function (err) {
                console.error('error during read ', err);
                res.status = 404;
                res.end();
            });

        } else {
            console.log(' is not a file ');
            res.status = 404;
            res.end();
        }
    });
}

fileServer.on('error', function (err) {

});