let net = require('net'),
    util = require('util'),
    eventEmitter = require('events').EventEmitter;

let Parser = require('./request/request_parser').RequestParser,
    Request = require('./request/request_parser').Request,
    Response = require('./response/response').Response,
    AggregateRequestHeaders = require('./request/aggregate_request_headers');

global.SOCKET_TIMEOUT = 20000;

function FileServer(requestListener) {
    if (typeof requestListener === 'function') {
        this.on('processRequest', requestListener);
    }

    this.tcpServer = net.createServer({ allowHalfOpen: false });
    this.tcpServer.on('connection', processSocketRequest.bind(this));

    return this;
}
util.inherits(FileServer, eventEmitter);

FileServer.prototype.listen = function (portNumber) {
    this.tcpServer.listen(portNumber);
    return this;
};

function processSocketRequest(socket) {
    let _self = this;
    console.log(' processSocketRequest ' );
    socket.setTimeout(SOCKET_TIMEOUT);
    let aggregateRequestHeaders = new AggregateRequestHeaders(socket);

    socket.on('data', function (data) {
        socket.pause();
        console.log(' received data ');
        socket.setTimeout(SOCKET_TIMEOUT);

        aggregateRequestHeaders.addHeadersChunk(data);

        aggregateRequestHeaders.on('receivedHeaders', function () {
            let requestHeaders = new Parser(aggregateRequestHeaders.requestHeaders).parse();
            _self.emit('processRequest', requestHeaders, new Response(socket, requestHeaders));
        });
    });

    socket.on('close', function () {
        console.log(' socket connection is closed ');
    });

    socket.on('end', function () {
        console.log(' socket connection is end ');
    });

    socket.on('error', function (err) {
        console.log(' error socket ', err);
    });

    socket.on('timeout', function () {
        console.log(' timeout on socket ');
        socket.end();
    });
}

module.exports = {
    FileServer,
    Request,
    Response
};
