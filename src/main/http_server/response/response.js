let Socket = require('net').Socket;
let Request = require('../request/request_parser').Request;

function Response(socket, request) {
    if (!(socket instanceof Socket) && !(request instanceof Request)) {
        throw new Error('Unsupported input params');
    }
    this.request = request;
    this.socket = socket;
    this.headers = new Map();
}

Response.prototype.writeHead = function (header, value) {
    if (typeof header === 'string' && typeof value === 'string') {
        this.headers.set(header, value);
    }
};

Response.prototype.writeFile = function (file) {
    this.file = file;
    let socket = this.socket;
    this.file.on('data', function () {
        socket.setTimeout(SOCKET_TIMEOUT);
    })
};

Response.prototype.end = function () {
    if (!this.statusCode && typeof this.statusCode !== 'number') {
        throw new Error('Response status is invalid');
    }

    let httpVersion = this.request.protocol;
    this.socket.write(httpVersion + ' ' + this.statusCode + '\n');
    for (let [header, value] of this.headers.entries()) {
        this.socket.write(header + ': ' + value + '\n');
    }
    this.socket.write('\n');
    if (this.file) {
        this.file.pipe(this.socket);
    } else {
        console.log(' closed socket ' );
        this.socket.end();
    }
};

module.exports = {Response};