let util = require('util');
let eventEmitter = require('events').EventEmitter;

const REQUEST_HEADERS_MAX_SIZE = 80 * 1024;

function AggregateRequestHeaders(timeout) {
    this.requestHeaders = new Buffer(0);
    this.timeout = 5000;
    if (timeout) {
        this.timeout = timeout;
    }
}
util.inherits(AggregateRequestHeaders, eventEmitter);

AggregateRequestHeaders.prototype.addHeadersChunk = function (headerChunk) {
    console.log(' received new header chunk ' );

    this.requestHeaders = Buffer.concat([this.requestHeaders, new Buffer(headerChunk, 'binary')]);

    setSocketTimeout.call(this);

    if (maxHeadersSizeExceeded(this.requestHeaders)) {
        console.log(' emit receivedHeaders exceeded ');
        this.emit('receivedHeaders');
    }
};

function setSocketTimeout() {
    if (this.socketTimeout) {
        clearTimeout(this.socketTimeout);
        this.socket.resume();
    }

    this.socketTimeout = setTimeout(() => {
        console.log(' emit receivedHeaders timeout');
        this.emit('receivedHeaders');
    }, this.timeout);
}

function maxHeadersSizeExceeded(requestHeaders) {
    return requestHeaders.length > REQUEST_HEADERS_MAX_SIZE;
}

module.exports = AggregateRequestHeaders;