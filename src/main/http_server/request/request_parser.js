function Request(generalInfo, headers) {
    this.method = generalInfo[1];
    this.uri = generalInfo[2].split('?')[0];
    this.protocol = generalInfo[3];
    this.headers = headers;
}

function RequestParser(requestData) {
    console.log(' requestData data ');
    if (!(requestData instanceof Buffer)) {
        console.error(' Invalid type. Should be Buffer ');
        throw new Error(' Invalid type. Should be Buffer ');
    }

    let requestHeaders = requestData.toString('ascii');
    let endOfRequestHeaders = requestHeaders.search(/\n\r\n/);
    requestHeaders = requestHeaders.slice(0, endOfRequestHeaders);

    this.headers = requestHeaders.match(/^.+$/gm);

    return this;
}

RequestParser.prototype.parse = function () {
    if (!this.headers) {
        new Error('Invalid request headers');
    }

    let generalHeader = this.headers[0];
    let generalInfo = generalHeader.match(/^(\S+)\s(\/\S*)\s(.*?)?$/);

    if (generalInfo.length !== 4) {
        throw new Error('Invalid request headers');
    }
    this.headers = this.headers.slice(1, this.headers.length);
    return new Request(generalInfo, this.headers);
};

module.exports = {
    RequestParser,
    Request
};